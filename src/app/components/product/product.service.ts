import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { Product } from './product.model';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  baseUrl = "http://localhost:8080/api/usuario/listarTodos"
  baseUrl2 = "http://localhost:8080/api/usuario/salvar"
  baseUrl3 = "http://localhost:8080/api/usuario"
  baseUrl4 = "http://localhost:8080/api/usuario/alterar"

  //baseUrl = "https://conraweb2.azurewebsites.net/api/usuario/listarTodos"
  //baseUrl2 = "https://conraweb2.azurewebsites.net/api/usuario/salvar"
  //baseUrl3 = "https://conraweb2.azurewebsites.net/api/usuario"
  //baseUrl4 = "https://conraweb2.azurewebsites.net/api/usuario/alterar"


  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }

  showMessage(msg: string): void {
      this.snackBar.open(msg, 'X', {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "top"
      })
  }

  create(product: Product): Observable<Product> {
     return this.http.post<Product>(this.baseUrl2, product)
  }

  read(): Observable<Product[]> {
     return this.http.get<Product[]>(this.baseUrl) 
  }

  readById(id: any): Observable<Product> {
     const url = `${this.baseUrl3}/${id}`
     return this.http.get<Product>(url)
  }

  update(product: Product): Observable<Product> {
     return this.http.put<Product>(this.baseUrl4,product)

  }
  
  delete(id: any): Observable<Product> {
   const url = `${this.baseUrl3}/${id}`
   return this.http.delete<Product>(url)
  }
}
